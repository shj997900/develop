import datetime
import pandas as pd
from matplotlib import pyplot as plt
from recurrent import RecurringEvent
from dateutil import rrule

date_1 = datetime.datetime.now()
print(date_1)
print(date_1.__repr__())
print(type(date_1))

date_2 = datetime.date.today()
print(date_2)
print(date_2.__repr__())
print(type(date_2))

print(pd.Timestamp(date_1))
print(pd.to_datetime(date_1))

date_3 = pd.Timestamp(1993, 6, 7, 15, 16, 0)
print(date_3)

print(date_1)
date_1 = pd.Timestamp(date_1)
print(date_1)
print(date_1.normalize())

TODAY = pd.Timestamp('today').normalize()
print(TODAY)
END = TODAY + datetime.timedelta(days=365)
print(END)

calendar = pd.DataFrame(index=pd.date_range(start=TODAY, end=END))

print(calendar.head())

print('Semi-month End:')
sm = pd.date_range(start=TODAY, end=END, freq='SM')
print(sm)
print('\n')
# month start frequency
print('Month Start:')
ms = pd.date_range(start=TODAY, end=END, freq='MS')
print(ms)

income = pd.DataFrame(
    data={'income': 1000},
    index=pd.date_range(start=TODAY, end=END, freq='SM')
)

print(income.head())

rent = pd.DataFrame(
    data={'rent': -1500},
    index=pd.date_range(start=TODAY, end=END, freq='MS')
)

print(rent.head())

calendar = pd.concat([calendar, income], axis=1).fillna(0)
calendar = pd.concat([calendar, rent], axis=1).fillna(0)
print(calendar.head(40))

print(calendar.loc[
    (calendar.index >= '2019-01-30') &
    (calendar.index <= '2019-02-02')
])

calendar['total'] = calendar.sum(axis=1)
calendar['cum_total'] = calendar['total'].cumsum()
print(calendar.tail(1))


plt.figure(figsize=(10, 5))
plt.plot(calendar.index, calendar.total, label='Daily Total')
plt.plot(calendar.index, calendar.cum_total, label='CumulativeTotal')
plt.legend()

plt.show()

bank = pd.DataFrame(
    data={'bank': 2000},
    index=pd.date_range(start=TODAY, end=TODAY)
)

print(bank)

calendar = pd.concat([calendar, bank], axis=1).fillna(0)

def update_totals(df):
    # check to see if these columns exist in our dataframe
    if df.columns.isin(['total', 'cum_total']).any():
        # if they do exist set them to 0
        df['total'] = 0
        df['cum_total'] = 0
    # recalculate total and cumulative_total
    df['total'] = df.sum(axis=1)
    df['cum_total'] = df['total'].cumsum()
    return df

calendar = update_totals(calendar)

print(calendar.tail(1))

def plot_budget(df):
    plt.figure(figsize=(10, 5))
    plt.plot(df.index, df.total, label='Daily Total')
    plt.plot(df.index, df.cum_total, label='Cumulative Total')
    plt.legend()

plot_budget(calendar)
plt.show()

vacation = pd.DataFrame(
    data={'vacation': -2500},
    index=[pd.Timestamp('2018-11-01').normalize()]
)

print(vacation)

calendar = pd.concat([calendar, vacation], axis=1).fillna(0)

calendar = update_totals(calendar)

plot_budget(calendar)
plt.show()

calendar = calendar.drop('vacation', axis=1)

frequency = 'every week until July 10th'

r = RecurringEvent()
r.parse(frequency)

rr = rrule.rrulestr(r.get_RFC_rrule())
rr.between(TODAY, END)

[pd.to_datetime(date).normalize() for date in rr.between(TODAY, END)]

