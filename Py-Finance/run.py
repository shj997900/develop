import pandas as pd
from scipy.optimize import newton
import matplotlib.pyplot as plt
from pandas import Series

def xnpv(rate, values, dates):
    '''Replicates the XNPV() function'''
    min_date = min(dates)
    return sum([
        value / (1 + rate)**((date - min_date).days / 365)
        for value, date
        in zip(values, dates)
    ])

def xirr(values, dates):
    return newton(lambda r: xnpv(r, values, dates), 0)

df = pd.read_csv('transactions.csv')

df['Date'] = pd.to_datetime(df['Date'])

#grouped = df.groupby('Category')

transaction_pivot = df.pivot_table(index='Date', columns='Transaction Type', values='Amount')

#transaction_pivot = df.pivot_table(index='Date', columns='Category', values='Amount')

transaction_pivot['Total'] = transaction_pivot.credit - transaction_pivot.debit

print(transaction_pivot)

transaction_pivot.reset_index(level=0, inplace=True)

tp = transaction_pivot.fillna(0)

tp['Total'] = tp['Total'].round(2)

print(tp)

print(xnpv(0.05, tp.Total, tp.Date))

tp['Total'].plot.line()

plt.show()


