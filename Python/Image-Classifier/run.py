import fastai as fai


def get_data(sz):
    data = ImageDataBunch.from_csv(
        path=path, csv_labels='train.csv'
        test='test', ds_tmfs=get_transforms(),
        size=sz, bs=32).normalize(imagenet_stats)

# training a resnet50 model on image size (64, 64)
learner_64 = cnn_learner(get_data(64), models.resnet50, metrics=accuracy)

# initialising the learner with previously saved weights & training on larger image size
learner_128 = cnn_learner(get_data(128), models.resnet50, metrics=accuracy)
load('learner_64')