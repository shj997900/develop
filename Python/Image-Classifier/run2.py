from fastai import *
from fastai.vision import *

mnist = untar_data(URLs.MNIST_TINY)
exit()
tfms = get_transforms(do_flip=False)

data = (ImageList.from_folder(mnist)
        .split_by_folder()
        .label_from_folder()
        .transform(tfms, size=32)
        .databunch()
        .normalize(imagenet_stats))

data.show_batch()
