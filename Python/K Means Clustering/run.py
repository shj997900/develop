import numpy as np
from scipy.cluster.vq import vq

obs = np.random.random(90).reshape(30,3)
print(obs)

c1 = np.random.choice(range(len(obs)))
c2 = np.random.choice(range(len(obs)))

clust_cen = np.vstack([obs[c1],obs[c2]])
print(clust_cen)

vq = vq(obs, clust_cen)
print(vq)

from scipy.cluster.vq import kmeans
kmeans(obs,clust_cen)

print(kmeans(obs, 2))