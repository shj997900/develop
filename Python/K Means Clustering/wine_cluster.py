import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import AgglomerativeClustering

df = pd.read_csv('wine.csv', sep=';')
print(df.head())

plt.hist(df['quality'])
plt.show()

quality_gb = df.groupby('quality').mean()
print(quality_gb)

# Normalize the data

df_norm = (df - df.min()) / (df.max() - df.min())
print(df_norm.head())

ward = AgglomerativeClustering(n_clusters=6, linkage='ward').fit(df_norm)
md=pd.Series(ward.labels_)

plt.hist(md)
plt.title('Histogram of Cluster Label')
plt.xlabel('Cluster')
plt.ylabel('Frequency')
plt.show()