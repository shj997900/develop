import pandas as pd
from io import StringIO

csv_data = \
    '''A,B,C,D
       1.0,2.0,3.0,4.0
       5.0,6.0,,8.0
       10.0,11.0,12.0,'''

df = pd.read_csv(StringIO(csv_data))
print(df)

# Count missing values in dataframe
print(df.isnull().sum())

# Remove rows that have NA values
print(df.dropna(axis=0))

# Remove columns that have NA values
print(df.dropna(axis=1))

print(df.dropna(how='all'))

# Drop rows that have less than 4 real values
print(df.dropna(thresh=4))

# Only drop rows where NaN appear in specific columns
print(df.dropna(subset=['C']))

from sklearn.preprocessing import Imputer

imr = Imputer(missing_values='NaN', strategy='mean', axis=0)
imr = imr.fit(df.values)

imputed_data = imr.transform(df.values)
print(imputed_data)