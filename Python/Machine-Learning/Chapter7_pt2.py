import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from sklearn import linear_model
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import roc_curve, auc


from sklearn.datasets import load_breast_cancer

cancer = load_breast_cancer()
x = cancer.data[:,0]    # mean radius
y = cancer.target      # 0: malignant, 1: benign
colors = {0:'red', 1:'blue'}   # 0: malignant, 1: benign

plt.scatter(x,y,
            facecolors='none',
            edgecolors=pd.DataFrame(cancer.target)[0].apply(lambda x: colors[x]),
            cmap=colors)

plt.xlabel("mean radius")
plt.ylabel("Result")

red = mpatches.Patch(color='red', label='malignant')
blue = mpatches.Patch(color='blue', label='benign')

plt.legend(handles=[red, blue], loc=1)

plt.show()

log_regress = linear_model.LogisticRegression()

# train the model
log_regress.fit(X = np.array(x).reshape(len(x),1), y = y)

print(log_regress.intercept_)

print(log_regress.coef_)

def sigmoid(x):
    return (1 / (1 +
                 np.exp(-(log_regress.intercept_[0] + (log_regress.coef_[0] [0] * x)))))

x1 = np.arange(0, 30, 0.01)
y1 = [sigmoid(n) for n in x1]

plt.scatter(x, y,
            facecolors='none', edgecolors=pd.DataFrame(cancer.target) [0].apply(lambda x: colors[x]),
            cmap=colors)

plt.plot(x1,y1)
plt.xlabel("mean radius")
plt.ylabel("Probability")
plt.show()

print(log_regress.predict_proba([[20]]))
print(log_regress.predict([[20]])[0])  # 0

print(log_regress.predict_proba([[8]]))
print(log_regress.predict([[8]])[0])


train_set, test_set, train_labels, test_labels = train_test_split(
    cancer.data,
    cancer.target,
    test_size= 0.25,
    random_state = 1,
    stratify= cancer.target
)

x = train_set[:,0:30]
y = train_labels
log_regress = linear_model.LogisticRegression()
log_regress.fit(X = x,
                y = y)

print(log_regress.intercept_)
print(log_regress.coef_)

preds_prob = pd.DataFrame(log_regress.predict_proba(X=test_set))

preds_prob.columns = ["Malignant", "Benign"]

preds = log_regress.predict(X=test_set)
preds_class = pd.DataFrame(preds)
preds_class.columns = ["Prediction"]

original_result = pd.DataFrame(test_labels)
original_result.columns = ["Original Result"]

result = pd.concat([preds_prob, preds_class, original_result], axis=1)
print(result.head())

print("---Confusion Matrix---")
print(pd.crosstab(preds, test_labels))

print(metrics.confusion_matrix(y_true = test_labels,  # True labels
                                    y_pred = preds))       # Predicted labels

print("---Accuracy---")
print(log_regress.score(X=test_set,
                        y=test_labels))

print(metrics.classification_report(y_true = test_labels, y_pred = preds))

#---find the predicted probabilities using the test set
probs = log_regress.predict_proba(test_set)
preds = probs[:,1]

#---find the FPR, TPR, and threshold----
fpr, tpr, threshold = roc_curve(test_labels, preds)

print(fpr)
print(tpr)
print(threshold)

roc_auc = auc(fpr, tpr)

plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate (TPR)')
plt.xlabel('False Positive Rate (FPR)')
plt.title('Receiver Operating Characteristic (ROC)')
plt.legend(loc= 'lower right')
plt.show()