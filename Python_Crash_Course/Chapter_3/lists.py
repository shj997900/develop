bicyles = ['trek', 'cannondale', 'redline', 'specialized']
print(bicyles[-1].title())

message = f"My first bicycle was a {bicyles[2].title()}"
print(message)

motorcycles = ['honda', 'yamaha', 'suzuki']
print(motorcycles)

motorcycles[0] = 'ducati'
print(motorcycles)

motorcycles.append('harley')
print(motorcycles)

motorcycles.insert(2, 'yamaha')
print(motorcycles)

del motorcycles[2]
print(motorcycles)

'''popped_motorcyle = motorcycles.pop()
print(motorcycles)
print(popped_motorcyle)'''

last_owned = motorcycles.pop()
print(f"The last motorcyle I owned was a {last_owned.title()}.")

first_owned = motorcycles.pop(0)
print(f"The first motorcyle I owned was a {first_owned.title()}.")

motorcycles.remove('yamaha')
print(motorcycles)

cars = ['bmw', 'audi', 'toyota', 'subaru']
cars.sort()
print(cars)

cars.sort(reverse=True)
print(cars)

cars = ['bmw', 'audi', 'toyota', 'subaru']
print("\nHere is the sorted list:")
print(sorted(cars,reverse=True))
print("\nHere is the original list again:")
print(cars)

cars = ['bmw', 'audi', 'toyota', 'subaru']
cars.reverse()
print(cars)
cars.reverse()