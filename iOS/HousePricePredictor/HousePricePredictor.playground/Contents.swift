import CreateML
import Foundation

let trainingLoc = URL(fileURLWithPath: "/Users/Molly/Desktop/Dataset/train.csv")

let houseData = try MLDataTable(contentsOf: trainingLoc)

let (trainingData, testData) = houseData.randomSplit(by:0.8, seed: 0)

let model = try MLRegressor(trainingData: trainingData, targetColumn: "SalePrice")

//let evaluation =     model.testingMetrics(on:testData)

let modeldata = MLModelMetadata(author: "Stephen Harle", shortDescription: "Model that predicts house prices", license: "Open Source",     version: "1.0")

try model.write(to: URL(fileURLWithPath: "/Users/Molly/Desktop/HousePricePredictor.mlmodel"), metadata: modeldata)




