import CreateML
import Foundation

let trainingLoc = URL(fileURLWithPath: "/Users/Molly/Desktop/MyDataset/")
let testLoc = URL(fileURLWithPath: "/Users/Molly/Documents/Github/develop/iOS/Text_Sentiment_Classifier/MyTestDataset")

let model = try MLTextClassifier(trainingData: .labeledDirectories(at: trainingLoc))

try model.write(to: URL(fileURLWithPath: "/Users/Molly/Desktop/SentimentAnalyzer.mlmodel"))
